package File;

import java.io.*;

public class reverseFile {

    public static void main(String[] args) throws IOException {
        FileInputStream fileInputStream = new FileInputStream("C:\\Users\\jalalshokouh\\Desktop\\Newfolder\\22-4-FileOutputStream1.WMV");
        BufferedInputStream bis = new BufferedInputStream(fileInputStream,8*1024);
        long start = System.nanoTime();
        byte[] arrayByte = new byte[bis.available()];
        bis.read(arrayByte);

        byte[] reverseArray= new byte[arrayByte.length];
        for (int i =0 ;i<=arrayByte.length-1 ; i++){
            reverseArray[i]=arrayByte[arrayByte.length-i-1];
        }
        FileOutputStream fileOutputStream = new FileOutputStream ("C:\\Users\\jalalshokouh\\Desktop\\Newfolder\\22-4-FileOutputStream1.WMV");
        BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream,8*1024);
        bos.write(reverseArray);
        fileInputStream.close();
        fileOutputStream.close();
        bos.flush();
        bis.close();
        bos.close();
        long ent = System.nanoTime();
        System.out.println(ent-start);
    }

}
