//Given a string s and an array of strings words, determine whether s is a prefix string of words.
//
//        A string s is a prefix string of words if s can be made by concatenating the first k strings in words for some positive k no larger than words.length.
//
//        Return true if s is a prefix string of words, or false otherwise.
package package1;

public class Main {

    public static void main(String[] args) {
        System.out.println(isPrefix("iloveleetcode", new String[]{"i", "love", "leetcode", "apple"}));

    }

    public static boolean isPrefix(String s , String[]worlds){
        String sum= "";
        for (int i=0; i<worlds.length; i++) {
            sum+=worlds[i];
            if (sum.length()>=s.length())
                break;
        }
        System.out.println("s.length:"+s.length()+"\tsum.length:"+sum.length());
        return sum.startsWith(s);
    }

}