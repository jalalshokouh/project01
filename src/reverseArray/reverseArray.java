package reverseArray;

public class reverseArray {
    public static void reverseArray (int[] array ,int low, int high){
        if (low<high) {
            int temp = array[low];
            array[low] = array[high];
            array[high] = temp;
            reverseArray(array, low + 1, high - 1);
        }

    }
    public static void main(String[] args ) {
        int[] data ={1,6,9,8,7,5,6,9,4};
        reverseArray(data,0,data.length-1);
        for (int a:data) {
                  System.out.println(a);
        }

    }
}
