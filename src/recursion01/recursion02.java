package recursion01;

public class recursion02 {
    public static void main(String [] args){
        System.out.println(Power(15,10));
    }
    public static long Power(int a , int b){
        if (b==0) {
            return 1;
        }
        else
            return a*Power(a,b-1);
    }
}
