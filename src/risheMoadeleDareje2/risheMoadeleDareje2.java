//حل معادله درجه دو
package risheMoadeleDareje2;
import java.util.Scanner;
import java.text.MessageFormat;
public class risheMoadeleDareje2 {
    public static void main (String[] args){
        System.out.println("Enter the Coefficient like sample : ax^2+bx+c=0");
        Scanner input = new Scanner(System.in);
        System.out.print("a :");
        float a = input.nextInt();
        System.out.print("b :");
        float b = input.nextInt();
        System.out.print("c :");
        float c = input.nextInt();
        double delta = b*b-(4*a*c);
        if (delta>=0) {
            double x1 = (-b + Math.sqrt(delta)) / 2 * a;
            double x2 = (-b - Math.sqrt(delta)) / 2 * a;
            if (b < 0 && c >= 0)
                System.out.println(MessageFormat.format("{0}x^2{1}x+{2}=0 \n x1 = {3}\n x2={4}", a, b, c, x1, x2));
            else if (c < 0 && b >= 0)
                System.out.println(MessageFormat.format("{0}x^2+{1}x{2}=0 \n x1 = {3}\n x2={4}", a, b, c, x1, x2));
            else if (b < 0 && c < 0)
                System.out.println(MessageFormat.format("{0}x^2{1}x{2}=0 \n x1 = {3}\n x2={4}", a, b, c, x1, x2));
            else
                System.out.println(MessageFormat.format("{0}x^2+{1}x+{2}=0 \n x1 = {3}\n x2={4}", a, b, c, x1, x2));
        }
        else
            System.out.println("your equation has no roots ");

    }
}
