package JDBC;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {

    public static void main(String[] args) {
        try {
            java.sql.Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc","root","");
            Statement statement = connection.createStatement();
            statement.executeUpdate("INSERT INTO student (firstName, lastName, studentId) VALUE ('jamal','shokouh',545651)");
            ResultSet resultSet = statement.executeQuery("select * from student");
            while (resultSet.next()){
                System.out.println(resultSet.getString("firstName"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
