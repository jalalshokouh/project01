package AddTwoNumbers;

import java.util.LinkedList;

public class Solution {
    LinkedList<Integer> l1 = new LinkedList<Integer>();
    LinkedList<Integer> l2 = new LinkedList<Integer>();
    LinkedList<Integer> l3 = new LinkedList<Integer>();
    int counter = 0;

    public Solution(LinkedList<Integer> l1, LinkedList<Integer> l2) {
        this.l1.addAll(l1);
        this.l2.addAll(l2);
    }

    public void solution() {
        for (int i = 0; i < l1.size(); i++) {
            int sum = l1.get(i) + l2.get(i);
            sum += counter;
            if (sum < 10) {
                l3.add(sum);
                counter = 0;
            } else {
                l3.add(sum - 10);
                counter = 0;
                counter++;
            }
        }
        if (l3.getLast()==0)
            l3.removeLast();
        for (int a : l3) {
            System.out.println(a);
        }
    }
}
