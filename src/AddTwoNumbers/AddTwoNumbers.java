package AddTwoNumbers;

import java.util.LinkedList;

public class AddTwoNumbers {
    public static void main(String[] args) {
        LinkedList<Integer> l1 = new LinkedList<Integer>();
        LinkedList<Integer> l2 = new LinkedList<Integer>();
        LinkedList<Integer> l3 = new LinkedList<Integer>();
        l1.add(7);
        l1.add(8);
        l1.add(6);
        l2.add(3);
        l2.add(6);
        l2.add(0);
        l1.add(8);
        if (l1.size() == l2.size()) {
            l1.add(0);
            l2.add(0);
            Solution s1 = new Solution(l1,l2);
            s1.solution();
        }
            else if (l1.size() > l2.size()) {
                    int temp = l1.size() - l2.size();
                    for (int j = 0; j < temp; j++)
                        l2.add(0);
            Solution s1 = new Solution(l1,l2);
                    s1.solution();

                } else {
                    int temp = l2.size() - l1.size();
                    for (int j = 0; j < temp; j++)
                        l1.add(0);
            Solution s1 = new Solution(l1,l2);
                    s1.solution();
                }
            }
    }
