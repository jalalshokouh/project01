package method;
import java.util.Scanner;
public class method01 {
    public static void main (String[] args){
        Scanner input = new Scanner(System.in);
        System.out.println("enter string");
        String a = input.next();
        System.out.println("enter integer");
        int x = input.nextInt();
        System.out.println(additional(a,x));
    }
    static String additional (String a , int x){
        if (x > a.length())
            return "";
        else
            return a.substring(0,x);
    }
}
