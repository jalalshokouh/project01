package method;

public class method02 {
    public static void main (String[] args){
        System.out.println(isDouble("54"));

    }
    public static double isDouble(String s){
        double result=0;
        for (int i=0 ; i<s.length() ; i++) {
            if (s.charAt(i) < '0' || s.charAt(i) > '9') {
                if (s.charAt(i)!='.')
                result = -1;
            }
            if (result==-1)break;
            }
            if (result!=-1) {
                double res = Double.parseDouble(s);
                result = res;
             }
            return result;
    }
}
